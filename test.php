<?php
require __DIR__ . '/vendor/autoload.php';

$key	=	'sdgkjdnfkhgnhljfd';

// $child	=	new	class()	extends	\Fork\Child {
// 	protected	function	execute() {
// 		global	$key;

// 		$this->send('hallo from child '.$key.PHP_EOL);
// 		echo	$this->receive();
// 		sleep(2);
// 		echo	'Child'.PHP_EOL;
// 	}
// };

$child	=	new	\Fork\Child(function($me) {
	global	$key;

	$me->send('hallo from child '.$key.PHP_EOL);
	echo	$me->receive();
	sleep(2);
	echo	'Child'.PHP_EOL;
	$me->send('end of child '.PHP_EOL);
});

$child->send('hallo from parent'.PHP_EOL);
echo	$child->receive();
do {
	echo	'Parent'.PHP_EOL;
	sleep(1);
} while($child->status());
var_dump($child);
echo	$child->receive();
unset($child);

sleep(100);
?>