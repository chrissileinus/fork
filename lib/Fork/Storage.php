<?php
/* Fork\Storage.php - Class to fork a process and manage the children
 * Copyright (C) 2020 Christian Backus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* File Authors:
 *   Christian Backus <chrissileinus@googlemail.com>
 */

namespace	Fork;

class	Storage {
	protected	$sem				=	null;
	protected	$shm				=	null;
	protected	$size				=	null;
	protected	$name				=	null;
	protected	$locked			=	false;
	protected	$keepLocked	=	false;

	/**	__construct	START	-------------------------------------------------------
	 * $name		name of the Storage
	 */
	final	public		function	__construct(string $name, int $size = 1024) {
		$this->size		=	$size;
		$this->name		=	$name;
		$this->sem		=	new	Semaphor($this->name);
		$this->shm		=	new	SharedMemory($this->name, $this->size);

		if($this->shm->first()) {
			$this->write([]);
		}
	}
	//	__construct	END		-------------------------------------------------------

	/**	lock	START	-------------------------------------------------------
	 */
	final	private		function	lock() {
		if(!$this->locked && !$this->keepLocked) {
			$this->locked	=	$this->sem->lock();
		}
	}
	//	lock	END		-------------------------------------------------------

	/**	unlock	START	-------------------------------------------------------
	 */
	final	private		function	unlock() {
		if($this->locked && !$this->keepLocked) {
			$this->locked	=	!$this->sem->unlock();
		}
	}
	//	unlock	END		-------------------------------------------------------

	/**	size	START	-------------------------------------------------------
	 */
	final	public		function	size() {
		return	$this->shm->size();
	}
	//	size	END		-------------------------------------------------------

	/**	usage	START	-------------------------------------------------------
	 */
	final	public		function	usage() {
		return	$this->shm->usage();
	}
	//	usage	END		-------------------------------------------------------

	/**	doLocked	START	-------------------------------------------------------
	 */
	final	public		function	doLocked(callable $call, ...$data) {
		$this->keepLocked	=	$this->sem->lock();
		$result	=	$call($this, ...$data);
		$this->keepLocked	=	!$this->sem->unlock();

		return	$result;
	}
	//	unlock	END		-------------------------------------------------------

	/**	read	START	-------------------------------------------------------
	 */
	final	private		function	read() {
		return	unserialize($this->shm->read());
	}
	//	read	END		-------------------------------------------------------

	/**	write	START	-------------------------------------------------------
	 */
	final	private		function	write($tmp) {
		$tmp	=	serialize($tmp);
		if(strlen($tmp) > $this->size) {
			while(strlen($tmp) > $this->size) {
				$this->size	=	$this->size + 1024;
			}

			$this->shm->delete();
			$this->shm		=	new	Storage\SharedMemory($this->name, $this->size);
		}

		return	$this->shm->write($tmp);
	}
	//	write	END		-------------------------------------------------------

	/**	offsetSet	START	-------------------------------------------------------
	 */
	final	public		function	__set($offset,	$data) {
		$this->lock();

		$tmp	=	$this->read();
		if(is_null($offset)) {
			$tmp[]				=	$data;
		}
		else {
			$tmp[$offset]	=	$data;
		}

		$this->write($tmp);

		$this->unlock();
	}
	//	offsetSet	END		-------------------------------------------------------

	/**	offsetUnset	START	-------------------------------------------------------
	 */
	final	public		function	__unset($offset) {
		$this->lock();

		$tmp	=	$this->read();
		unset($tmp[$offset]);

		$this->write($tmp);

		$this->unlock();
	}
	//	offsetUnset	END		-------------------------------------------------------

	/**	offsetGet	START	-------------------------------------------------------
	 */
	final	public		function	__get($offset) {
		$this->lock();
		$tmp	=	$this->read();
		$this->unlock();

		return	isset($tmp[$offset])?
			$tmp[$offset]:
			null;
	}
	//	offsetGet	END		-------------------------------------------------------

	/**	offsetExists	START	-------------------------------------------------------
	 */
	final	public		function	__isset($offset) {
		$this->lock();
		$tmp	=	$this->read();
		$this->unlock();

		return	isset($tmp[$offset]);
	}
	//	offsetExists	END		-------------------------------------------------------
}

?>