<?php
/**	Zerlegt einen Zeitstempel
*			@param		Float	$timestamp				Der Zeitstempel als Gleitkommazahl
*			@param		Int		$expo					Der Exponent
*			@return		Array							Array mit den Bestandteilen der Gleitkommazahl
*/
function	splitFloatTimestamp(float $timestamp, int $expo = -3) {
	if($expo > 0)			$expo	=	ceil(log($expo) / log(10)) * -1;

	$int	=	intval($timestamp);
	return	[
		$int,
		intval(floatval($timestamp-$int) / (10 ** $expo))
	];
}
?>