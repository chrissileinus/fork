<?php
/* Fork\Pool.php - Class to fork a process and manage the children
 * Copyright (C) 2020 Christian Backus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* File Authors:
 *   Christian Backus <chrissileinus@googlemail.com>
 */

namespace	Fork;

class	Pool {
	protected	$children				=	[];
	protected	$worker					=	null;
	protected	$prepare				=	null;
	protected	$maxChildren		=	null;
	protected	$pidParent			=	null;
	protected	$socketPool			=	null;
	protected	$socketChildren	=	null;
	protected	$lock						=	null;
	protected	$busyCount			=	0;

	/**	__construct	START	-------------------------------------------------------
	 */
	final	public		function	__construct($worker, $prepare = null, int $maxChildren = null) {
		$this->worker				=	$worker;
		$this->prepare			=	$prepare;
		$this->maxChildren	=	$maxChildren;
		$this->pidParent		=	getmypid();
		$this->lock					=	new Semaphor(uniqid(true));
		list($this->socketPool, $this->socketChildren)	=	\stream_socket_pair(STREAM_PF_UNIX, STREAM_SOCK_STREAM, STREAM_IPPROTO_IP);
		// \stream_set_blocking($this->socketPool, false);
		// \stream_set_blocking($this->socketChildren, false);
	}
	//	__construct	END		-------------------------------------------------------

	//	__destruct	START	-------------------------------------------------------
	final	public		function	__destruct() {
		$this->shutdown();
	}
	//	__destruct	END		-------------------------------------------------------

	/**	submit		START	-------------------------------------------------------
	* submit a job into the pool
	*/
	final	public		function	submit($data, float $timeout = 5) {
		//	Start Children
		if(is_null($this->maxChildren) || count($this->children) < $this->maxChildren) {
			$this->children[]	=	new	Child(function($child) {
				//	START	Running in the child process
				do {
					pcntl_signal_dispatch();
					if(!@stream_get_meta_data($this->socketChildren)) {
						throw	new	Exception("Lost connection!");
					}

					$data = $this->accept();
					if($data !== false) {
						$child->busy(true);

						if(is_callable($this->worker))			$result	=	call_user_func($this->worker, $child, $data);
						elseif($this->worker instanceof worker)	$result	=	$this->worker->run($child, $data);
						else									throw	new	Exception("Worker ist not valid!");

						if(!is_null($result) && !is_bool($result))
							$child->send($result);

						$child->busy(false);
					}
				} while(true);
				//	END		Running in the child process
			}, $this->prepare);
		}

		//	Submit data to a Child
		if(!@stream_get_meta_data($this->socketPool)) {
			throw	new	Exception("Lost connection!");
		}

		$sleep	=	$timeout;
		$read		=	null;
		$write	=	[$this->socketPool];
		$except	=	null;
		if(@\stream_select($read, $write, $except, 0, $sleep * 1000000) > 0) {
			return	stream_socket_sendto($this->socketPool, json_encode($data).Config::EOT);
		}

		return	false;
	}
	//	submit		END		-------------------------------------------------------

	/**	accept		START	-------------------------------------------------------
	* accept submited data
	*/
	final	private		function	accept() {
		$this->lock->lock();
		$buffer	=	null;
		$read		=	[$this->socketChildren];
		$write	=	null;
		$except	=	null;
		if(@\stream_select($read, $write, $except, 1) > 0) {
			$buffer	=	stream_get_line($this->socketChildren, Config::LENGTH, Config::EOT);
		}
		$this->lock->unlock();

		if(is_null($buffer))	return	false;

		return	json_decode($buffer, true);
	}
	//	accept		END		-------------------------------------------------------

	/**	collect		START	-------------------------------------------------------
	* submit a job into the pool
	*/
	final	public		function	collect(callable $collector = null) {
		$count	=	0;
		foreach($this->children as $key => $child) {
			if(is_callable($collector)) {
				$result	=	call_user_func($collector, $child);
			}
			else {
				$result	=	$child->status();
			}
			if($result)		$count++;
			if(!$result) {
				// var_dump($this->children[$key]);
				unset($this->children[$key]);
			}
		}
		// return	count($this->children);
		return	$count;
	}
	//	collect		END		-------------------------------------------------------

	//	shutdown	START	-------------------------------------------------------
	final	public		function	shutdown(int $signal = SIGTERM) {
		if($this->pidParent !== getmypid())	return;	//	not allowed to run this in a child.

		foreach($this->children as $key => $child) {
			$result	=	$child->shutdown($signal);
			if(!$result)	unset($this->children[$key]);
		}
	}
	//	shutdown	START	-------------------------------------------------------

	//	busy	START	-------------------------------------------------------
	final	public		function	busy() {
		if(count($this->children) < $this->maxChildren)	return	false;
		foreach($this->children as $child) {
			if(!$child->busy())	return	false;
		}
		return	true;
	}
	//	busy	START	-------------------------------------------------------

	//	getAllReadSockets	START	-------------------------------------------------------
	final	public		function	getAllReadSockets() {
		$sockets	=	[];
		foreach($this->children as $child) {
			$sockets[]	=	$child->socket();
		}
		return	$sockets;
	}
	//	getAllReadSockets	START	-------------------------------------------------------
}

?>