<?php
/* Fork\Massage.php - Class to fork a process and manage the children
 * Copyright (C) 2020 Christian Backus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* File Authors:
 *   Christian Backus <chrissileinus@googlemail.com>
 */

namespace	Fork;

class	Message {
	protected	$msg	=	null;

	/**	__construct	START	-------------------------------------------------------
	 * $key		name of the Storage
	 */
	final	public		function	__construct(string $name) {
		$this->msg		=	msg_get_queue(intval(sprintf("%u", crc32($name))));
	}
	//	__construct	END		-------------------------------------------------------

	/**	__destruct	START	-------------------------------------------------------
	 */
	final	public		function	__destruct() {
	}
	//	__destruct	END		-------------------------------------------------------

	/**	send	START	-------------------------------------------------------
	 */
	final	public		function	send($message, int $type = 1) {
		return	msg_send($this->msg, $type, $message);
	}
	//	send	END		-------------------------------------------------------

	/**	receive	START	-------------------------------------------------------
	 */
	final	public		function	receive(int $type = 1) {
		if(msg_receive($this->msg, 0, $type, Config::LENGTH, $message)) {
			return	$message;
		}
		return	false;
	}
	//	receive	END		-------------------------------------------------------
}

?>