<?php
/* Fork\init.php - Class to fork a process and manage the children
 * Copyright (C) 2020 Christian Backus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* File Authors:
 *   Christian Backus <chrissileinus@googlemail.com>
 */

namespace	Fork;

class	Child {
	protected	$socket		=	false;
	protected	$pid			=	0;
	protected	$started	=	null;
	protected	$running	=	null;
	protected	$busy		=	null;
	protected	$error		=	[];
	protected	$signal		=	[];

	/**	__construct	START	-------------------------------------------------------
	 * $execute is called in the child process.
	 *
	 * $prepare is called after the fork in both processes.
	 * In example to reconnect a database connection.
	 */
	final	public		function	__construct(callable $execute, callable $prepare = null) {
		$this->busy	=	new	SharedMemory(uniqid(true));
		list($socketPaternt, $socketChild)	=	\stream_socket_pair(STREAM_PF_UNIX, STREAM_SOCK_STREAM, STREAM_IPPROTO_IP);
		\stream_set_blocking($socketChild, false);
		\stream_set_blocking($socketPaternt, false);

		$pid	=	\pcntl_fork();
		if($pid < 0) {
			throw	new	Exception("fork error");
		}
		elseif($pid > 0) {	//	Parent
			fclose($socketChild);
			$this->socket		=	$socketPaternt;
			$this->pid			=	$pid;

			// echo	'init Parent'.$this->pid.PHP_EOL;
			$this->started	=	true;
			$this->running	=	true;

			if(is_callable($prepare)) {
				call_user_func($prepare, $this);
			}
		}
		else {				//	New child
			fclose($socketPaternt);
			$this->socket	=	$socketChild;
			$this->pid		=	getmypid();
			$this->busy(true);

			// echo	'init Child '.$this->pid.PHP_EOL;
			$this->signal();
			if(is_callable($prepare)) {
				call_user_func($prepare, $this);
			}

			if(is_callable($execute)) {
				call_user_func($execute, $this);
			}
			else {
				$this->execute();
			}
			$this->busy(false);
			exit;
		}
	}
	//	__construct	END		-------------------------------------------------------

	//	__destruct	START	-------------------------------------------------------
	final	public		function	__destruct() {}
	//	__destruct	END		-------------------------------------------------------

	//	shutdown	START	-------------------------------------------------------
	final	public		function	shutdown(int $signal = SIGTERM) {
		if($this->pid === getmypid())	throw	new	Exception("'shutdown' is not allowed to call in a child");
		posix_kill($this->pid, $signal);
		return	$this->status(true);
	}
	//	shutdown	START	-------------------------------------------------------

	//	signal		START	-------------------------------------------------------
	final	protected	function	signal() {
		foreach(Config::signalName() as $no => $name) {
			if(array_search($no, [9, 19]) === false)	pcntl_signal($no, SIG_DFL);
		}
		pcntl_signal(SIGUSR1, function () {});
		pcntl_signal(SIGUSR2, function () {});
		pcntl_signal(SIGPIPE, function () {});
		register_shutdown_function(function() {
			posix_kill($this->pid, SIGKILL);
		});
	}
	//	signal		END		-------------------------------------------------------

	//	busy		START	-------------------------------------------------------
	final	public		function	busy(bool $val = null) {
		if($this->pid === getmypid())	{
			return	$this->busy->write(serialize($val));
		}
		else {
			return	unserialize($this->busy->read());
		}
	}
	//	busy		END		-------------------------------------------------------

	/**	status		START	-------------------------------------------------------
	* check the status of child
	*/
	final	public		function	status($block = false) {
		if($this->running === false)	return	false;
		if($this->pid === getmypid())	throw	new	Exception("'status' is not allowed to call in a child");

		if($block)	$result	=	pcntl_waitpid($this->pid, $status);
		else				$result	=	pcntl_waitpid($this->pid, $status, WNOHANG | WUNTRACED);

		if($result === -1) {
			throw	new	Exception("'pcntl_waitpid' failed");
		}
		elseif($result === 0) {
			$this->running	=	true;
		}
		else {
			if(pcntl_wifsignaled($status)) {
				$this->signal['no']		=	pcntl_wtermsig($status);
				$this->signal['name']	=	Config::signalName($status);
			}
			if(pcntl_wifstopped($status)) {
				$this->signal['stop']	=	pcntl_wstopsig($status);
				$this->signal['name']	=	Config::signalName($status);
			}
			if(pcntl_wifexited($status)) {
				$this->error['no']		=	pcntl_wexitstatus($status);
				$this->error['msg']		=	pcntl_strerror($this->error['no']);
			}
			else {
				$this->error['no']		=	pcntl_get_last_error();
				$this->error['msg']		=	pcntl_strerror($this->error['no']);
			}

			$this->running	=	false;
		}

		return	$this->running;
	}
	//	status		END		-------------------------------------------------------

	/**	isRunning	START	-------------------------------------------------------
	* check if running and return the result
	*/
	final	public		function	isRunning() {
		if($this->pid === getmypid())	throw	new	Exception("'isRunning' is not allowed to call in a child");
		return	$this->status();
	}
	//	isRunning	END		-------------------------------------------------------

	/**	wait		START	-------------------------------------------------------
	* wait vor a signal of a timeout
	*/
	final	public		function	wait(float $timeout = 0) {
		if($this->pid !== getmypid())	throw	new	Exception("'wait' is not allowed to call in a parent");

		list($tSec, $tNsec)	=	_fn::splitFloatTimestamp($timeout, -9);
		pcntl_sigtimedwait(array_keys(Config::signalName()), $info, $tSec, $tNsec);
		if(array_search($info['signo'], Config::WAKEUPSIG) === false)	exit;
		return	$info;
	}
	//	send		END		-------------------------------------------------------

	/**	wakeup		START	-------------------------------------------------------
	* wake the child
	*/
	final	public		function	wakeup() {
		if($this->pid === getmypid())	throw	new	Exception("'wakeup' is not allowed to call in a child");

		return	posix_kill($this->pid, SIGUSR1);
	}
	//	send		END		-------------------------------------------------------

	/**	send		START	-------------------------------------------------------
	* Send data
	*/
	final	public		function	send($data) {
		if(is_array($data)) {
			$data	=	json_encode($data);
		}

		if(!@stream_get_meta_data($this->socket)) {
			throw	new	Exception("Lost connection!");
		}

		return	stream_socket_sendto($this->socket, $data.Config::EOT);
	}
	//	send		END		-------------------------------------------------------

	/**	receive		START	-------------------------------------------------------
	* Receive data
	*/
	final	public		function	receive() {
		if(!@stream_get_meta_data($this->socket)) {
			throw	new	Exception("Lost connection!");
		}

		$data	=	stream_get_line($this->socket, Config::LENGTH, Config::EOT);

		$array	=	json_decode($data, true);
		if(json_last_error() === JSON_ERROR_NONE)	$data	=	$array;

		return	$data;
	}
	//	receive		END		-------------------------------------------------------

	/**	getPid		START	-------------------------------------------------------
	* return the Process ID
	*/
	final	public		function	getPid() {
		return	$this->pid;
	}
	//	getPid		END		-------------------------------------------------------

	/**	socket		START	-------------------------------------------------------
	* return the socket
	*/
	final	public		function	socket() {
		return	$this->socket;
	}
	//	getPid		END		-------------------------------------------------------

	//	execute		START	-------------------------------------------------------
	protected	function	execute() {}
	//	execute		END		-------------------------------------------------------
}

?>