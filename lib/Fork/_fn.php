<?php
namespace Fork;

/**	Diese Klasse stellt die Möglichkeit zur Verfügung dynamisch Funktionen nach zu laden.
 *
 */
class	_fn {
	/**	Lädt eine Funktion aus einer PHP-Datei, führt diese mit den übergebenen Argumenten aus und gibt das Ergebnis zurück.
	 *			@param		Callable	$fn					Funktion die ausgeführt werden soll.
	 *			@param		Mixed		$args				Argumente die an die Funktion weiter gegeben werden.
	 *			@return		Mixed							Rückgabe der Werte die die nachgeladene Funktion zurück gibt.
	 */
	public	static	function	__callStatic($fn, $args) {
		if(!function_exists($fn)) {
			require	__DIR__ . DIRECTORY_SEPARATOR . 'fn' . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $fn) . '.php';
		}
		return	call_user_func_array($fn, $args);
	}
}
?>
