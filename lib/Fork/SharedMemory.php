<?php
/* Fork\SharedMemory.php - Class to fork a process and manage the children
 * Copyright (C) 2020 Christian Backus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* File Authors:
 *   Christian Backus <chrissileinus@googlemail.com>
 */

namespace	Fork;

class	SharedMemory {
	protected	$shm	=	null;
	protected	$key	=	null;
	protected	$size	=	null;

	/**	__construct	START	-------------------------------------------------------
	 * $key		name of the Storage
	 */
	final	public		function	__construct(string $name, int $size = 1024) {
		$this->key	=	intval(sprintf("%u", crc32($name)));
		$this->size	=	$size;

		$this->init();
	}
	//	__construct	END		-------------------------------------------------------

	/**	__destruct	START	-------------------------------------------------------
	 */
	final	public		function	__destruct() {
		shmop_close($this->shm);
	}
	//	__destruct	END		-------------------------------------------------------

	/**	init	START	-------------------------------------------------------
	 */
	final	private		function	init() {
		$this->shm		=	shmop_open($this->key, 'c', 0666, $this->size);
		if(!$this->shm) {
			$this->shm		=	shmop_open($this->key, 'w', 0, 0);
			$tmp	=	$this->read();
			shmop_delete($this->shm);
			$this->shm		=	shmop_open($this->key, 'c', 0666, $this->size);
			$this->write($tmp);
		}
	}
	//	init	END		-------------------------------------------------------

	/**	first	START	-------------------------------------------------------
	 */
	final	public		function	first() {
		$tmp	=	$this->read();

		if(strlen(str_replace(chr(0), '', $tmp) == 0))	return	true;

		return	false;
	}
	//	first	END		-------------------------------------------------------

	/**	delete	START	-------------------------------------------------------
	 */
	final	public		function	delete() {
		return	shmop_delete($this->shm);
	}
	//	delete	END		-------------------------------------------------------

	/**	read	START	-------------------------------------------------------
	 */
	final	public		function	read() {
		return	shmop_read($this->shm, 0, 0);
	}
	//	read	END		-------------------------------------------------------

	/**	size	START	-------------------------------------------------------
	 */
	final	public		function	size() {
		return	shmop_size($this->shm);
	}
	//	size	END		-------------------------------------------------------

	/**	usage	START	-------------------------------------------------------
	 */
	final	public		function	usage() {
		return	strlen(trim(shmop_read($this->shm, 0, 0)));
	}
	//	usage	END		-------------------------------------------------------

	/**	write	START	-------------------------------------------------------
	 */
	final	public		function	write(string $data) {
		return	shmop_write($this->shm, $data, 0);
	}
	//	write	END		-------------------------------------------------------
}

?>