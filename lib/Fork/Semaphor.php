<?php
/* Fork\Semaphor.php - Class to fork a process and manage the children
 * Copyright (C) 2020 Christian Backus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* File Authors:
 *   Christian Backus <chrissileinus@googlemail.com>
 */

namespace	Fork;

class	Semaphor {
	protected	$sem	=	null;

	/**	__construct	START	-------------------------------------------------------
	 * $key		name of the Storage
	 */
	final	public		function	__construct(string $name) {
		$this->sem		=	sem_get(intval(sprintf("%u", crc32($name))));
	}
	//	__construct	END		-------------------------------------------------------

	/**	__destruct	START	-------------------------------------------------------
	 */
	final	public		function	__destruct() {
		@sem_release($this->sem);
	}
	//	__destruct	END		-------------------------------------------------------

	/**	lock	START	-------------------------------------------------------
	 */
	final	public		function	lock(int $wait = -1) {
		if($wait < 0)	return	sem_acquire($this->sem);

		$end	=	microtime(true) + ($wait/1000);
		do {
			if(sem_acquire($this->sem, true))	return	true;
			usleep(1000);
		} while($end > microtime(true));

		return	false;
	}
	//	lock	END		-------------------------------------------------------

	/**	unlock	START	-------------------------------------------------------
	 */
	final	public		function	unlock() {
		return	sem_release($this->sem);
	}
	//	unlock	END		-------------------------------------------------------
}

?>