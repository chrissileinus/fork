<?php
/* Fork/Config.php - Class to fork a process and manage the children
 * Copyright (C) 2020 Christian Backus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* File Authors:
 *   Christian Backus <chrissileinus@googlemail.com>
 */

/* Parts:
 *   class Config
 */

namespace	Fork;

class	Config {
	const		EOT				=	'\r\nEOT\n\r';
	// const		LENGTH		=	8192;		//	8*1024
	const		LENGTH		=	1048576;		//	1024*1024
	const		TIMEOUT		=	10;

	const		WAKEUPSIG	=	[SIGUSR1, SIGUSR2];

	static	public		function	signalName(int $signal = null) {
		static	$signalNames	=	null;
		if(!$signalNames)
		foreach (get_defined_constants(true)['pcntl'] as $name => $num) {
			if(preg_match('/^SIG[^_]\S*/', $name)) {	// the _ is to ignore SIG_IGN and SIG_DFL and SIG_ERR and SIG_BLOCK and SIG_UNBLOCK and SIG_SETMARK, and maybe more, who knows
				$signalNames[$num]	=	$name;
			}
		}

		if(is_null($signal))	return	$signalNames;
		else									return	$signalNames[$signal];
	}
}
?>