<?php
require __DIR__ . '/vendor/autoload.php';

$key	=	'sdgkjdnfkhgnhljfd';

$store	=	new	\Fork\Storage('testPool', 1024);

$pool	=	new	\Fork\Pool(function($child, $data) {
	global	$store;

	$child->send('hallo from child '.$child->getPid().' '.$data);
	$store->doLocked(function($store, $data) {
		$tmp	=	$store->one;
		$tmp[]	=	$data.' - '.$data;
		$store->one	=	$tmp;
	}, $data);

	while($received = $child->receive()) {
		echo	'child  > '.$received.PHP_EOL;
	}

	$child->send('end of child ');

	return	md5($data);
}, null, 2);

$count	=	0;
do {
	$pool->submit('test1');
	$pool->submit('test2');
	$pool->submit('test3');
	$pool->submit('test4');
	sleep(1);
	$count++;
} while(
	$count < 10 &&
	$pool->collect(function($child) {
		$child->send('hallo from parent');
		while($data	=	$child->receive()) {
			echo	'parent > '.$data.PHP_EOL;
		}

		$child->wakeup();

		return	$child->status();
	}) !== false
);

// var_dump($pool);
var_dump($store->one);
var_dump([$store->size(), $store->usage()]);

?>